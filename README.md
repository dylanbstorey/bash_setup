# Dot Files

Use as you wish , install through :

```bash
wget -q -O - https://gitlab.com/dylanbstorey/bash_setup/raw/master/install.sh | bash 
```


# Other things to install

Minimal Ubuntu setup

```bash
sudo snap install pycharm-professional --classic
sudo add-apt-repository ppa:noobslab/icons2
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install terminator mdadm unity-tweak-tools python3.7 python3.8 uniform-icons
```

# Docker

```bash
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get install     apt-transport-https     ca-certificates     curl     gnupg-agent     software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io 
sudo usermod -aG docker $USER
```

# CUDA/NVIDIA Drivers


```bash
docker volume ls -q -f driver=nvidia-docker | xargs -r -I{} -n1 docker ps -q -a -f volume={} | xargs -r docker rm -f
sudo apt-get purge -y nvidia-docker
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey |   sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list |   sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update
sudo apt-get install -y nvidia-docker2
curl -s -L https://developer.nvidia.com/compute/cuda/10.1/Prod/local_installers/cuda-repo-ubuntu1604-10-1-local-10.1.168-418.67_1.0-1_amd64.deb
sudo dpkg -i cuda-repo-ubuntu1604-10-1-local-10.1.168-418.67_1.0-1_amd64.deb
sudo apt-key add /var/cuda-repo-10-1-local-10.1.168-418.67/7fa2af80.pub
sudo apt-get update
sudo apt-get install cuda
```

# Sublime preferences

### Install Packages:

- DocBlockr
- GitGutter
- PackageControl
- ReStructured Text Improved
- SideBar
- Virtualenv
- Sublime Python IDE
- OmniMarkup Preview

```bash
{
    "ignored_packages":
    [
        "RestructuredText",
        "Vintage"
    ],
    "translate_tabs_to_spaces": true,
    "hot_exit" : false,
    "remember_open_files" : false,
    "theme" : "Adaptive.sublime-theme",
    "trim_trailing_white_space_on_save" : true

}
```