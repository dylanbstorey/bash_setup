#!/bin/bash
#
# Utility Functions

venv_create(){
###########################################################
# Create a virtual environment
# Globals:
#    None
# Arguments:
#    name - what to name the environment (required)
#    python - which binary to use (default: python3)
# Returns:
#    0 - clean 
#    1 - name not provided
###########################################################

  local name="${1}"
  local python="${2:-"python3"}"

  if [ -z "$name" ]; then
    echo "virtual environment name needs to be set" >&2
    echo "proper usage : create_venv <name> <python binary>" >&2
    return 1
  else
    python -m virtualenv -p "$python" ~/.venv/"$name"
  fi
}

venv_activate(){
###########################################################
# Activate a created virtual environment
# Globals:
#    None
# Arguments:
#    name - which virtual environment to activate
# Returns:
#    None
###########################################################
  local name="${1}"

  if [ -z "$name" ]; then 
    echo "virtual environment name required" >&2
    echo "proper usage: activate_venv <name>" >&2
    return 1
  else
    source ~/.venv/"$name"/bin/activate
  fi
}

_venv_activate()
############################################################
# Sets autocompletion on venv_activate to be all environments in ~/.venv
# Globals:
#    COMPREPLY
# Arguments:
#    None
# Returns:
#    None
############################################################
{
    local cur=${COMP_WORDS[COMP_CWORD]}
    local availabl_venvs=$(ls -l ~/.venv| grep -v total | tr -s ' ' | cut -d ' ' -f 9 | tr '\n' ' ')
    COMPREPLY=( $(compgen -W "${availabl_venvs}" -- $cur) )
}

complete -F _venv_activate venv_activate
venv_deactivate(){
###########################################################
# Deactivate a currently active environment
# Globals:
#    None
# Arguments:
#    None
# Returns:
#    None
##########################################################
  deactivate
}

venv_list(){
###########################################################
# List currently available virtual environments
# Globals:
#    None
# Arguments:
#    None
# Returns:
#    List of environments
##########################################################
  echo "Available environments: "
  ls -l ~/.venv| grep -v total | tr -s ' ' | cut -d ' ' -f 9 | sed -e 's/^/  /'
}