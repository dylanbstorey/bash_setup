#!/bin/bash
#
# Install script for all this mess



backup(){
#########################################################
# Function that backsup certain known files if they exist
# Globals:
#     None
# Arguments:
#     None
# Returns:
#     None
#########################################################

  if [[ -f ~/.bashrc ]]; then 
    cp ~/.bashrc ~/.bashrc.bak
  fi

  if [[ -f ~/.bash_profile ]]; then
    cp ~/.bash_profile ~/.bash_profile.bak
  fi

}

bash_installs(){
#########################################################
# Copy basic basic bash stuff over
# Globals:
#     None
# Arguments:
#     None
# Returns:
#     None
#########################################################
  wget -O  ~/.bash_aliases.sh https://gitlab.com/dylanbstorey/bash_setup/raw/master/bash_aliases.sh
  wget -O ~/.prompt.sh https://gitlab.com/dylanbstorey/bash_setup/raw/master/prompt.sh
  wget -O ~/.bash_profile https://gitlab.com/dylanbstorey/bash_setup/raw/master/bash_profile.sh
  wget -O ~/.bashrc https://gitlab.com/dylanbstorey/bash_setup/raw/master/bashrc.sh
  wget -O ~/local_aliases.tmp https://gitlab.com/dylanbstorey/bash_setup/raw/master/local_aliases.sh
  cat ~/local_aliases.tmp >> ~/.local_aliases.sh 
}



if [[ "$OSTYPE" == "linux-gnu" ]]; then
  backup
  bash_installs
  wget -O ~/.git_functions.sh https://gitlab.com/dylanbstorey/bash_setup/raw/master/git_functions.sh
  wget -O ~/.venv_functions.sh https://gitlab.com/dylanbstorey/bash_setup/raw/master/venv_functions.sh
  source ~/.bash_profile
  source ~/.bash_profile
elif [[ "$OSTYPE" == "darwin"* ]]; then
  backup
  bash_installs
  wget -O ~/.git_functions.sh https://gitlab.com/dylanbstorey/bash_setup/raw/master/git_functions.sh
  wget -O ~/.venv_functions.sh https://gitlab.com/dylanbstorey/bash_setup/raw/master/venv_functions.sh
  source ~/.bash_profile
  source ~/.bashrc
elif [[ "$OSTYPE" == "cygwin" ]]; then
  # POSIX compatibility layer and Linux environment emulation for Windows
  echo "$OSTYPE not supported at this time." >&2
elif [[ "$OSTYPE" == "msys" ]]; then
  # Lightweight shell a nd GNU utilities compiled for Windows (part of MinGW)
  echo "$OSTYPE not supported at this time." >&2
elif [[ "$OSTYPE" == "win32" ]]; then
  echo "$OSTYPE not supported at this time." >&2
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  echo "$OSTYPE not supported at this time." >&2
  # ...
fi

rm local_aliases.tmp
